<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (\Bitrix\Main\Loader::includeModule('iblock'))
{

	$result=array();

	function convertDate ($d)
	{
		$_monthsList = array(
			  "января " => ".01.",
			  "февраля" => ".02.",
			  "марта" => ".03.",
			  "апреля" => ".04.",
			  "мая"  => ".05." ,
			  "июня"  => ".06." ,
			  "июля"  => ".07.",
			  "августа"  => ".08.",
			  "сентября" => ".09." ,
			  "октября"  => ".10.",
			  "ноября"  => ".11.",
			  "декабря"  => ".12."
			);
			
			$md=explode(" ", $d);
			
			return $md[0].$_monthsList[$md[1]].$md[2];
	}

	function parseText ($url)
	{
		$content = file_get_contents($url);

		$content=explode("Новости</h1>", $content);
		$content=explode('<ul class="pager"', $content[1]);
		$content=explode("</li>", $content[0]);


		foreach ($content as $li)
		{	
			
			if (preg_match('#  <li>
		            
					<span class="date">([^<]+)</span>
					
					<a href="([^<]+)">
		                <h3>([^<]+)</h3>
		            </a>#su',$li,$list)) 
					{

						$code=explode("/", $list[2]);
						
						$result_massiv[]=array(
						"DATE"	=>  convertDate( $list[1] ),
						"URL"	=> "https://teatrium.ru".$list[2],
						"NAME"	=> $list[3],
						"CODE" => $code[3]
						);
						
					}
			
		}

		return $result_massiv;

	}

	function parseDetailText ($url)
	{
		$content = file_get_contents($url);

		$content=explode("</h1>", $content);
		$content=explode('<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8">', $content[1]);

		return $content[0];
	}


	$finish = 110;
	for ($i=1;$i<=$finish; $i++)
	{
		$result=array_merge(parseText("https://***.ru/theater/news/page/".$i."/"), $result);	
	}

	foreach ($result as &$elem)
	{
		$elem["DETAIL_TEXT"]=parseDetailText($elem["URL"]);
	}


	//added elements fot ib
	foreach ($result as $elem)
	{

		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      		=> 18,
		  "NAME"           		=> $elem["NAME"],
		  "DATE_ACTIVE_FROM"	=> $elem["DATE"],  
		  "CODE"        		=> $elem["CODE"], 
		  "ACTIVE"         		=> "Y",
		  "DETAIL_TEXT"    		=> $elem["DETAIL_TEXT"]
		  );

		if($PRODUCT_ID = $el->Add($arLoadProductArray))
		  echo "New ID: ".$PRODUCT_ID;
		else
		  echo "Error: ".$el->LAST_ERROR;
	}
}
